FROM maven:3-jdk-8

EXPOSE 8080

COPY target/hello-world-1.0-SNAPSHOT.jar hello-world-1.0-SNAPSHOT.jar

ENTRYPOINT ["java", "-jar","hello-world-1.0-SNAPSHOT.jar"]
